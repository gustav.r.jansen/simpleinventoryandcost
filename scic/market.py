class Market(object):
    def __init__(this, name):
        this.name = name

    def __eq__(this, other):
        if other is None: return False
        return this.name == other.name

    def __ne__(this, other): return not this==other

    def __str__(this): return this.name
    def __hash__(this): return hash(this.name)
