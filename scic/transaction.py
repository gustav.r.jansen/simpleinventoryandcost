import itertools
import datetime

class Transaction(object):
    counter = itertools.count()

    def __init__(this, timestamp):

        assert isinstance(timestamp, datetime.datetime)

        this.id = next(Transaction.counter)
        this.timestamp = timestamp
