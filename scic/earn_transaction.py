from . import Transaction
from . import Ticker
from . import Market

class EarnTransaction(Transaction):
    def __init__(this, timestamp, ticker, market, amount):

        assert isinstance(ticker, Ticker)
        assert isinstance(market, Market)
        assert isinstance(amount, (int, float))

        super().__init__(timestamp)
        this.ticker = ticker
        this.market = market
        this.amount = amount
