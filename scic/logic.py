import datetime
from . import TransactionRegistry, Inventory, InventoryBatch, Ticker, HistoricalPrices
from . import Transaction, EarnTransaction, LostTransaction

class Logic(object):
    def __init__(this, registry, inventory, prices):

        assert isinstance(registry, TransactionRegistry)
        assert isinstance(inventory, Inventory)
        assert isinstance(prices, HistoricalPrices)

        this.registry = registry
        this.inventory = inventory
        this.prices = prices

    def create_taxable_events_if_necessary(this, transaction, added, removed):
        pass

    def apply_transaction_to_inventory(this, transaction):
        assert isinstance(transaction, Transaction)

        added = []
        removed = []
        if isinstance(transaction, EarnTransaction):
            added = this.add_transaction_to_inventory(transaction)

        elif isinstance(transaction, LostTransaction):
            removed = this.remove_transaction_from_inventory(transaction)

        this.create_taxable_events_if_necessary(transaction, added, removed)

    def add_transaction_to_inventory(this, transaction):

        if isinstance(transaction, EarnTransaction):
            batches = this.create_batches_from_earn(transaction)

        this.inventory.add_batches(batches)

        return batches

    def remove_transaction_from_inventory(this, transaction):
        if isinstance(transaction, LostTransaction):
            market, ticker, amount = this.create_args_from_lost(transaction)

        return this.inventory.remove_and_return_by_amount(market, ticker, amount)

    def create_batches_from_earn(this, transaction):
        assert isinstance(transaction, EarnTransaction)

        return [InventoryBatch(transaction.timestamp, transaction.ticker, transaction.amount,
                this.prices.get_price(transaction.ticker, transaction.timestamp), transaction.market)]

    def create_args_from_lost(this, lost):
        assert isinstance(lost, LostTransaction)

        return lost.market, lost.ticker, lost.amount
