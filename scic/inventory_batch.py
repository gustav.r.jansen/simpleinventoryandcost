import itertools

class InventoryBatch(object):
    counter = itertools.count()
    def __init__(this, timestamp, ticker, amount, unit_cost, market, removable=True):
        this.id = this.newid()
        this.timestamp = timestamp
        this.ticker = ticker
        this.amount = amount
        this.unit_cost = unit_cost
        this.market = market
        this.removable = removable

    def newid(this):
        return next(InventoryBatch.counter)

    def get_cost(this):
        return this.amount*this.unit_cost

    def remove_amount(this, amount):
        if this.removable:
            res = abs(min(0, this.amount - amount))
            this.amount = max(0, this.amount - amount)
        else:
            res= amount
        return res

    def remove_value(this, value):
        amount = float(value)/float(this.unit_cost)
        return this.remove_amount(float(value)/this.unit_cost)*this.unit_cost

    def __str__(this):
        return f"{this.timestamp}, {this.ticker}, {this.amount}, {this.unit_cost}, {this.market}, {this.removable}"

    @classmethod
    def from_other(cls, other, amount=None):
        new_amount = other.amount
        if  amount is not None: new_amount = amount
        return cls(other.timestamp, other.ticker, new_amount, other.unit_cost, other.market, other.removable)
