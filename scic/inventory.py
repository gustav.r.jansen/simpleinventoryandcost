from .inventory_batch import InventoryBatch

class Inventory(object):
    def __init__(this):
        this.structure = {}
        this.lookup = {}

    def get_number_of_batches(this, market=None, ticker=None):
        count = 0
        for t, val in this.structure.items():
            if ticker is not None and t != ticker: continue
            for m, l in val.items():
                if market is not None and m != market: continue
                count += len(l)

        return count

    def has_batches(this, market, ticker):
        if ticker not in this.structure.keys(): return False
        return market in this.structure[ticker].keys()

    def get_batches(this, market, ticker):
        if this.has_batches(market, ticker): return this.structure[ticker][market]
        else: return []

    def add_batches(this, batches):
        for b in batches: this.add_batch(b, False)
        this.sort_the_batches()

    def add_batch(this, batch, sort=True):
        if batch.ticker not in this.structure.keys():
            this.structure[batch.ticker] = {}

        if batch.market not in this.structure[batch.ticker].keys():
            this.structure[batch.ticker][batch.market] = []

        this.structure[batch.ticker][batch.market].append(batch)
        if sort: this.sort_the_batches()

        this.lookup[batch.id] = batch

    def sort_the_batches(this):
        for t, val in this.structure.items():
            for m in val.keys():
                this.structure[t][m].sort(key=lambda b:b.timestamp)

    def remove_and_return_oldest_batch(this, market, ticker):
        if ( this.has_batches(market, ticker) and len(this.structure[ticker][market]) > 0 ):
            return this.remove_and_return_batch(this.structure[ticker][market][0])

    def remove_and_return_batch(this, batch):
        if ( not this.has_batch(batch) ): return

        del this.lookup[batch.id]
        return this.structure[batch.ticker][batch.market].pop(
                this.structure[batch.ticker][batch.market].index(batch))

    def get_batch_by_id(this, myid):
        if myid in this.lookup: return this.lookup[myid]

    def remove_and_return_batch_by_id(this, myid):
        return this.remove_and_return_batch(this.get_batch_by_id(myid))

    def has_batch(this, batch):
        if batch is None: return False
        return batch.id in this.lookup

    def remove_and_return_by_amount(this, market, ticker, amount):
        result = []
        remaining_amount = amount
        while remaining_amount > 0:
            batch = this.remove_and_return_oldest_batch(market, ticker)
            if batch is None:
                raise ValueError("Inventory empty but asked to remove more.")

            if batch.amount <= remaining_amount:
                remaining_amount -= batch.amount
                result.append(batch)
            else:
                this.add_batch(InventoryBatch.from_other(batch, batch.amount-remaining_amount))
                result.append(InventoryBatch.from_other(batch, remaining_amount))
                remaining_amount = 0
        return result
