class Ticker(object):
    def __init__(this, ticker):
        this.ticker = ticker.upper()

    def __eq__(this, other):
        if other is None: return False
        return this.ticker == other.ticker

    def __ne__(this, other): return not this==other

    def __str__(this): return this.ticker

    def __hash__(this): return hash(this.ticker)
