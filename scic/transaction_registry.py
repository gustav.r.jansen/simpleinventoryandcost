from  . import Transaction

class TransactionRegistry(object):
    def __init__(this):
        this.items = []

    def add_transactions(this, items):
        if type(items) is not list: this.add_transaction(items)
        else: this.items.extend(items)

        this.sort_transactions()

    def add_transaction(this, item, sort=True):
        this.items.append(item)
        if sort: this.sort_transactions()

    def sort_transactions(this):
        this.items.sort(key=lambda i:i.timestamp)

    def get_number_of_transactions(this): return len(this.items)
