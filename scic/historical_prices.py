from abc import ABC, abstractmethod

class HistoricalPrices(ABC):
    @abstractmethod
    def get_price(this, ticker, date):
        ...
