import datetime
import unittest
import scic

class TestTransactionRegistry(unittest.TestCase):
    def setUp(this):
        this.now = datetime.datetime.now(datetime.timezone.utc)
        this.now_p1 = this.now + datetime.timedelta(days=1)
        this.now_m1 = this.now - datetime.timedelta(days=1)
        this.now_m2 = this.now - datetime.timedelta(days=2)
        this.now_m3 = this.now - datetime.timedelta(days=3)
        this.now_m4 = this.now - datetime.timedelta(days=4)

    def test_add_transaction(this):
        registry = scic.TransactionRegistry()

        registry.add_transaction(scic.Transaction(this.now))
        this.assertEqual(registry.get_number_of_transactions(), 1)

        registry.add_transaction(scic.Transaction(this.now_m1))
        this.assertEqual(registry.get_number_of_transactions(), 2)
        this.assertEqual(registry.items[0].timestamp, this.now_m1)
        this.assertEqual(registry.items[1].timestamp, this.now)

        registry.add_transaction(scic.Transaction(this.now_m4))
        registry.add_transaction(scic.Transaction(this.now_m3))
        registry.add_transaction(scic.Transaction(this.now_p1))
        registry.add_transaction(scic.Transaction(this.now_m2))
        this.assertEqual(registry.get_number_of_transactions(), 6)
        this.assertEqual(registry.items[0].timestamp, this.now_m4)
        this.assertEqual(registry.items[1].timestamp, this.now_m3)
        this.assertEqual(registry.items[2].timestamp, this.now_m2)
        this.assertEqual(registry.items[3].timestamp, this.now_m1)
        this.assertEqual(registry.items[4].timestamp, this.now)
        this.assertEqual(registry.items[5].timestamp, this.now_p1)
if __name__ == '__main__':
    unittest.main()
