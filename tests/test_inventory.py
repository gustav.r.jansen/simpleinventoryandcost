import unittest
import scic
import datetime
import copy

class TestInventory(unittest.TestCase):
    def setUp(this):

        this.btc = scic.Ticker("btc")
        this.eth = scic.Ticker("eth")
        this.gemini = scic.Market("Gemini")
        this.binance = scic.Market("Binance")

        this.now = datetime.datetime.now(datetime.timezone.utc)
        this.now_p1 = this.now + datetime.timedelta(days=1)
        this.now_m1 = this.now - datetime.timedelta(days=1)
        this.now_m2 = this.now - datetime.timedelta(days=2)
        this.now_m3 = this.now - datetime.timedelta(days=3)

        this.inventory = scic.Inventory()
        this.inventory.add_batch(scic.InventoryBatch(this.now, this.btc, 2, 40000, this.gemini))
        this.inventory.add_batch(scic.InventoryBatch(this.now_m2, this.btc, 3, 50000, this.gemini))
        this.inventory.add_batch(scic.InventoryBatch(this.now_m3, this.eth, 1, 2000, this.gemini))
        this.inventory.add_batch(scic.InventoryBatch(this.now_m1, this.eth, 2, 2500, this.gemini))
        this.inventory.add_batch(scic.InventoryBatch(this.now_m1, this.btc, 4, 45000, this.binance))
        this.inventory.add_batch(scic.InventoryBatch(this.now, this.btc, 5, 47000, this.binance))
        this.inventory.add_batch(scic.InventoryBatch(this.now_m3, this.btc, 1.4, 41000, this.binance))
        this.inventory.add_batch(scic.InventoryBatch(this.now_m2, this.eth, 1.43, 1000, this.binance))
        this.inventory.add_batch(scic.InventoryBatch(this.now, this.eth, 1.45, 1400, this.binance))

    def test_create_empty_inventory(this):
        inventory = scic.Inventory()

        this.assertEqual(inventory.get_number_of_batches(), 0)
        this.assertEqual(inventory.get_number_of_batches(ticker=this.btc), 0)
        this.assertEqual(inventory.get_number_of_batches(market=this.gemini), 0)

    def test_add_batch(this):
        ticker = this.btc
        amount = 10.44
        unit_cost = 35034.45
        market = this.gemini
        batch = scic.InventoryBatch(this.now, ticker, amount, unit_cost, market)

        inventory = scic.Inventory()
        inventory.add_batch(batch)
        this.assertEqual(inventory.get_number_of_batches(), 1)
        this.assertEqual(inventory.get_number_of_batches(ticker=this.btc), 1)
        this.assertEqual(inventory.get_number_of_batches(market=this.gemini), 1)

        ticker = this.eth
        amount = 10.44
        unit_cost = 35034.45
        market = this.binance
        batch = scic.InventoryBatch(this.now, ticker, amount, unit_cost, market)
        inventory.add_batch(batch)

        this.assertEqual(inventory.get_number_of_batches(), 2)
        this.assertEqual(inventory.get_number_of_batches(ticker=this.btc), 1)
        this.assertEqual(inventory.get_number_of_batches(market=this.gemini), 1)
        this.assertEqual(inventory.get_number_of_batches(ticker=this.eth), 1)
        this.assertEqual(inventory.get_number_of_batches(market=this.binance), 1)

        old = this.now - datetime.timedelta(days=50)

        inventory = scic.Inventory()
        inventory.add_batch(scic.InventoryBatch(this.now, this.btc, amount, unit_cost, this.gemini))
        inventory.add_batch(scic.InventoryBatch(old, this.btc, amount, unit_cost, this.gemini))

        batches = inventory.get_batches(market=this.gemini, ticker=this.btc)
        this.assertEqual(len(batches), 2)
        this.assertTrue(batches[0].timestamp < batches[1].timestamp)

        inventory = scic.Inventory()
        inventory.add_batches(batches[::-1])
        this.assertEqual(inventory.get_number_of_batches(), 2)
        this.assertEqual(inventory.get_number_of_batches(ticker=this.btc), 2)
        this.assertEqual(inventory.get_number_of_batches(market=this.gemini), 2)

        batches = inventory.get_batches(market=this.gemini, ticker=this.btc)
        this.assertEqual(len(batches), 2)
        this.assertTrue(batches[0].timestamp < batches[1].timestamp)

    def test_add_and_remove_batches(this):

        inventory = scic.Inventory()
        inventory.add_batch(scic.InventoryBatch(this.now, this.btc, 2, 40000, this.gemini))
        inventory.add_batch(scic.InventoryBatch(this.now_m2, this.btc, 3, 50000, this.gemini))
        inventory.add_batch(scic.InventoryBatch(this.now_m3, this.eth, 1, 2000, this.gemini))
        inventory.add_batch(scic.InventoryBatch(this.now_m1, this.eth, 2, 2500, this.gemini))
        inventory.add_batch(scic.InventoryBatch(this.now_m1, this.btc, 4, 45000, this.binance))
        inventory.add_batch(scic.InventoryBatch(this.now, this.btc, 5, 47000, this.binance))
        inventory.add_batch(scic.InventoryBatch(this.now_m3, this.btc, 1.4, 41000, this.binance))
        inventory.add_batch(scic.InventoryBatch(this.now_m2, this.eth, 1.43, 1000, this.binance))
        inventory.add_batch(scic.InventoryBatch(this.now, this.eth, 1.45, 1400, this.binance))

        this.assertEqual(inventory.get_number_of_batches(), 9)
        this.assertEqual(inventory.get_number_of_batches(ticker=this.btc), 5)
        this.assertEqual(inventory.get_number_of_batches(market=this.gemini), 4)
        this.assertEqual(inventory.get_number_of_batches(ticker=this.eth), 4)
        this.assertEqual(inventory.get_number_of_batches(market=this.binance), 5)
        this.assertEqual(inventory.get_number_of_batches(ticker=this.btc, market=this.gemini), 2)
        this.assertEqual(inventory.get_number_of_batches(ticker=this.eth, market=this.gemini), 2)
        this.assertEqual(inventory.get_number_of_batches(ticker=this.btc, market=this.binance), 3)
        this.assertEqual(inventory.get_number_of_batches(ticker=this.eth, market=this.binance), 2)

        inventory.remove_and_return_oldest_batch(this.gemini, this.btc)
        this.assertEqual(inventory.get_number_of_batches(), 8)
        this.assertEqual(inventory.get_number_of_batches(ticker=this.btc), 4)
        this.assertEqual(inventory.get_number_of_batches(market=this.gemini), 3)
        this.assertEqual(inventory.get_number_of_batches(ticker=this.eth), 4)
        this.assertEqual(inventory.get_number_of_batches(market=this.binance), 5)
        this.assertEqual(inventory.get_number_of_batches(ticker=this.btc, market=this.gemini), 1)
        this.assertEqual(inventory.get_number_of_batches(ticker=this.eth, market=this.gemini), 2)
        this.assertEqual(inventory.get_number_of_batches(ticker=this.btc, market=this.binance), 3)
        this.assertEqual(inventory.get_number_of_batches(ticker=this.eth, market=this.binance), 2)

        batches = inventory.get_batches(this.gemini, this.btc)
        this.assertEqual(len(batches), 1)
        this.assertEqual(batches[0].timestamp, this.now)
        this.assertEqual(batches[0].amount, 2)
        this.assertEqual(batches[0].unit_cost, 40000)

    def test_remove_batch_from_empty_inventory(this):
        inventory = scic.Inventory()
        this.assertEqual(inventory.get_number_of_batches(), 0)
        inventory.remove_and_return_oldest_batch(this.gemini, this.btc)
        this.assertEqual(inventory.get_number_of_batches(), 0)

    def test_get_batch_by_id(this):
        inventory = scic.Inventory()
        inventory.add_batch(scic.InventoryBatch(this.now, this.btc, 2, 40000, this.gemini))
        inventory.add_batch(scic.InventoryBatch(this.now_m2, this.btc, 3, 50000, this.gemini))
        inventory.add_batch(scic.InventoryBatch(this.now_m3, this.eth, 1, 2000, this.gemini))
        inventory.add_batch(scic.InventoryBatch(this.now_m1, this.eth, 2, 2500, this.gemini))
        inventory.add_batch(scic.InventoryBatch(this.now_m1, this.btc, 4, 45000, this.binance))
        inventory.add_batch(scic.InventoryBatch(this.now, this.btc, 5, 47000, this.binance))
        inventory.add_batch(scic.InventoryBatch(this.now_m3, this.btc, 1.4, 41000, this.binance))
        inventory.add_batch(scic.InventoryBatch(this.now_m2, this.eth, 1.43, 1000, this.binance))
        inventory.add_batch(scic.InventoryBatch(this.now, this.eth, 1.45, 1400, this.binance))
        batch = scic.InventoryBatch(this.now_p1, this.btc, 0.3, 65000, this.gemini)
        inventory.add_batch(batch)

        batch2 = inventory.get_batch_by_id(batch.id)
        this.assertIsNotNone(batch2)
        this.assertIs(batch, batch2)

    def test_remove_specific_batch(this):
        inventory = scic.Inventory()
        inventory.add_batch(scic.InventoryBatch(this.now, this.btc, 2, 40000, this.gemini))
        inventory.add_batch(scic.InventoryBatch(this.now_m2, this.btc, 3, 50000, this.gemini))
        inventory.add_batch(scic.InventoryBatch(this.now_m3, this.eth, 1, 2000, this.gemini))
        inventory.add_batch(scic.InventoryBatch(this.now_m1, this.eth, 2, 2500, this.gemini))
        inventory.add_batch(scic.InventoryBatch(this.now_m1, this.btc, 4, 45000, this.binance))
        inventory.add_batch(scic.InventoryBatch(this.now, this.btc, 5, 47000, this.binance))
        inventory.add_batch(scic.InventoryBatch(this.now_m3, this.btc, 1.4, 41000, this.binance))
        inventory.add_batch(scic.InventoryBatch(this.now_m2, this.eth, 1.43, 1000, this.binance))
        inventory.add_batch(scic.InventoryBatch(this.now, this.eth, 1.45, 1400, this.binance))

        batch = scic.InventoryBatch(this.now_p1, this.btc, 0.3, 65000, this.gemini)
        inventory.add_batch(batch)

        this.assertEqual(inventory.get_number_of_batches(), 10)
        this.assertEqual(inventory.get_number_of_batches(ticker=this.btc), 6)
        this.assertEqual(inventory.get_number_of_batches(market=this.gemini), 5)
        this.assertEqual(inventory.get_number_of_batches(ticker=this.eth), 4)
        this.assertEqual(inventory.get_number_of_batches(market=this.binance), 5)
        this.assertEqual(inventory.get_number_of_batches(ticker=this.btc, market=this.gemini), 3)
        this.assertEqual(inventory.get_number_of_batches(ticker=this.eth, market=this.gemini), 2)
        this.assertEqual(inventory.get_number_of_batches(ticker=this.btc, market=this.binance), 3)
        this.assertEqual(inventory.get_number_of_batches(ticker=this.eth, market=this.binance), 2)


        this.assertTrue(inventory.has_batch(batch))

        inventory.remove_and_return_batch(batch)
        this.assertFalse(inventory.has_batch(batch))
        this.assertEqual(inventory.get_number_of_batches(), 9)
        this.assertEqual(inventory.get_number_of_batches(ticker=this.btc), 5)
        this.assertEqual(inventory.get_number_of_batches(market=this.gemini), 4)
        this.assertEqual(inventory.get_number_of_batches(ticker=this.eth), 4)
        this.assertEqual(inventory.get_number_of_batches(market=this.binance), 5)
        this.assertEqual(inventory.get_number_of_batches(ticker=this.btc, market=this.gemini), 2)
        this.assertEqual(inventory.get_number_of_batches(ticker=this.eth, market=this.gemini), 2)
        this.assertEqual(inventory.get_number_of_batches(ticker=this.btc, market=this.binance), 3)
        this.assertEqual(inventory.get_number_of_batches(ticker=this.eth, market=this.binance), 2)

    def test_remove_and_return_batch_by_id(this):
        inventory = scic.Inventory()
        inventory.add_batch(scic.InventoryBatch(this.now, this.btc, 2, 40000, this.gemini))
        inventory.add_batch(scic.InventoryBatch(this.now_m2, this.btc, 3, 50000, this.gemini))
        inventory.add_batch(scic.InventoryBatch(this.now_m3, this.eth, 1, 2000, this.gemini))
        inventory.add_batch(scic.InventoryBatch(this.now_m1, this.eth, 2, 2500, this.gemini))
        inventory.add_batch(scic.InventoryBatch(this.now_m1, this.btc, 4, 45000, this.binance))
        inventory.add_batch(scic.InventoryBatch(this.now, this.btc, 5, 47000, this.binance))
        inventory.add_batch(scic.InventoryBatch(this.now_m3, this.btc, 1.4, 41000, this.binance))
        inventory.add_batch(scic.InventoryBatch(this.now_m2, this.eth, 1.43, 1000, this.binance))
        inventory.add_batch(scic.InventoryBatch(this.now, this.eth, 1.45, 1400, this.binance))

        batch = scic.InventoryBatch(this.now_p1, this.btc, 0.3, 65000, this.gemini)
        inventory.add_batch(batch)
        batch2 = inventory.remove_and_return_batch_by_id(batch.id)
        this.assertIsNotNone(batch2)
        this.assertIs(batch, batch2)

        batch2 = inventory.remove_and_return_batch_by_id(batch.id)
        this.assertIsNone(batch2)

        this.assertEqual(inventory.get_number_of_batches(), 9)
        this.assertEqual(inventory.get_number_of_batches(ticker=this.btc), 5)
        this.assertEqual(inventory.get_number_of_batches(market=this.gemini), 4)
        this.assertEqual(inventory.get_number_of_batches(ticker=this.eth), 4)
        this.assertEqual(inventory.get_number_of_batches(market=this.binance), 5)
        this.assertEqual(inventory.get_number_of_batches(ticker=this.btc, market=this.gemini), 2)
        this.assertEqual(inventory.get_number_of_batches(ticker=this.eth, market=this.gemini), 2)
        this.assertEqual(inventory.get_number_of_batches(ticker=this.btc, market=this.binance), 3)
        this.assertEqual(inventory.get_number_of_batches(ticker=this.eth, market=this.binance), 2)

    def test_remove_amount_from_inventory(this):
        inventory = copy.deepcopy(this.inventory)

        # Remove a single batch by amount
        batches = inventory.remove_and_return_by_amount(this.gemini, this.btc, 3)
        this.assertIsNotNone(batches)
        this.assertTrue(type(batches) is list)
        this.assertEqual(len(batches), 1)
        this.assertEqual(batches[0].amount, 3)
        this.assertEqual(batches[0].timestamp, this.now_m2)
        this.assertEqual(batches[0].ticker, this.btc)
        this.assertEqual(batches[0].market, this.gemini)
        this.assertEqual(batches[0].unit_cost, 50000)
        this.assertTrue(batches[0].removable)

        this.assertEqual(inventory.get_number_of_batches(), 8)
        this.assertEqual(inventory.get_number_of_batches(ticker=this.btc), 4)
        this.assertEqual(inventory.get_number_of_batches(market=this.gemini), 3)
        this.assertEqual(inventory.get_number_of_batches(ticker=this.eth), 4)
        this.assertEqual(inventory.get_number_of_batches(market=this.binance), 5)
        this.assertEqual(inventory.get_number_of_batches(ticker=this.btc, market=this.gemini), 1)
        this.assertEqual(inventory.get_number_of_batches(ticker=this.eth, market=this.gemini), 2)
        this.assertEqual(inventory.get_number_of_batches(ticker=this.btc, market=this.binance), 3)
        this.assertEqual(inventory.get_number_of_batches(ticker=this.eth, market=this.binance), 2)

        # Try to remove more than exists in the inventory
        this.assertRaisesRegex(ValueError, "Inventory empty but asked to remove more.",
                inventory.remove_and_return_by_amount, this.gemini, this.btc, 3)

        inventory = copy.deepcopy(this.inventory)

        # Remove a partial batch
        batches = inventory.remove_and_return_by_amount(this.gemini, this.btc, 2)
        this.assertIsNotNone(batches)
        this.assertTrue(type(batches) is list)
        this.assertEqual(len(batches), 1)
        this.assertEqual(batches[0].amount, 2)
        this.assertEqual(batches[0].timestamp, this.now_m2)
        this.assertEqual(batches[0].ticker, this.btc)
        this.assertEqual(batches[0].market, this.gemini)
        this.assertEqual(batches[0].unit_cost, 50000)
        this.assertTrue(batches[0].removable)

        this.assertEqual(inventory.get_number_of_batches(), 9)
        this.assertEqual(inventory.get_number_of_batches(ticker=this.btc), 5)
        this.assertEqual(inventory.get_number_of_batches(market=this.gemini), 4)
        this.assertEqual(inventory.get_number_of_batches(ticker=this.eth), 4)
        this.assertEqual(inventory.get_number_of_batches(market=this.binance), 5)
        this.assertEqual(inventory.get_number_of_batches(ticker=this.btc, market=this.gemini), 2)
        this.assertEqual(inventory.get_number_of_batches(ticker=this.eth, market=this.gemini), 2)
        this.assertEqual(inventory.get_number_of_batches(ticker=this.btc, market=this.binance), 3)
        this.assertEqual(inventory.get_number_of_batches(ticker=this.eth, market=this.binance), 2)

        # Remove the remainder of the first batch
        batches = inventory.remove_and_return_by_amount(this.gemini, this.btc, 1)
        this.assertIsNotNone(batches)
        this.assertTrue(type(batches) is list)
        this.assertEqual(len(batches), 1)
        this.assertEqual(batches[0].amount, 1)
        this.assertEqual(batches[0].timestamp, this.now_m2)
        this.assertEqual(batches[0].ticker, this.btc)
        this.assertEqual(batches[0].market, this.gemini)
        this.assertEqual(batches[0].unit_cost, 50000)
        this.assertTrue(batches[0].removable)

        this.assertEqual(inventory.get_number_of_batches(), 8)
        this.assertEqual(inventory.get_number_of_batches(ticker=this.btc), 4)
        this.assertEqual(inventory.get_number_of_batches(market=this.gemini), 3)
        this.assertEqual(inventory.get_number_of_batches(ticker=this.eth), 4)
        this.assertEqual(inventory.get_number_of_batches(market=this.binance), 5)
        this.assertEqual(inventory.get_number_of_batches(ticker=this.btc, market=this.gemini), 1)
        this.assertEqual(inventory.get_number_of_batches(ticker=this.eth, market=this.gemini), 2)
        this.assertEqual(inventory.get_number_of_batches(ticker=this.btc, market=this.binance), 3)
        this.assertEqual(inventory.get_number_of_batches(ticker=this.eth, market=this.binance), 2)

        inventory = copy.deepcopy(this.inventory)

        # Remove more than a full batch, but not everything.
        batches = inventory.remove_and_return_by_amount(this.gemini, this.btc, 4)
        this.assertIsNotNone(batches)
        this.assertTrue(type(batches) is list)
        this.assertEqual(len(batches), 2)
        this.assertEqual(batches[0].amount, 3)
        this.assertEqual(batches[0].timestamp, this.now_m2)
        this.assertEqual(batches[0].ticker, this.btc)
        this.assertEqual(batches[0].market, this.gemini)
        this.assertEqual(batches[0].unit_cost, 50000)
        this.assertTrue(batches[0].removable)

        this.assertEqual(batches[1].amount, 1)
        this.assertEqual(batches[1].timestamp, this.now)
        this.assertEqual(batches[1].ticker, this.btc)
        this.assertEqual(batches[1].market, this.gemini)
        this.assertEqual(batches[1].unit_cost, 40000)
        this.assertTrue(batches[1].removable)

        this.assertEqual(inventory.get_number_of_batches(), 8)
        this.assertEqual(inventory.get_number_of_batches(ticker=this.btc), 4)
        this.assertEqual(inventory.get_number_of_batches(market=this.gemini), 3)
        this.assertEqual(inventory.get_number_of_batches(ticker=this.eth), 4)
        this.assertEqual(inventory.get_number_of_batches(market=this.binance), 5)
        this.assertEqual(inventory.get_number_of_batches(ticker=this.btc, market=this.gemini), 1)
        this.assertEqual(inventory.get_number_of_batches(ticker=this.eth, market=this.gemini), 2)
        this.assertEqual(inventory.get_number_of_batches(ticker=this.btc, market=this.binance), 3)
        this.assertEqual(inventory.get_number_of_batches(ticker=this.eth, market=this.binance), 2)

        # Remove the remainder of the last batch
        batches = inventory.remove_and_return_by_amount(this.gemini, this.btc, 1)
        this.assertIsNotNone(batches)
        this.assertTrue(type(batches) is list)
        this.assertEqual(len(batches), 1)
        this.assertEqual(batches[0].amount, 1)
        this.assertEqual(batches[0].timestamp, this.now)
        this.assertEqual(batches[0].ticker, this.btc)
        this.assertEqual(batches[0].market, this.gemini)
        this.assertEqual(batches[0].unit_cost, 40000)
        this.assertTrue(batches[0].removable)

        this.assertEqual(inventory.get_number_of_batches(), 7)
        this.assertEqual(inventory.get_number_of_batches(ticker=this.btc), 3)
        this.assertEqual(inventory.get_number_of_batches(market=this.gemini), 2)
        this.assertEqual(inventory.get_number_of_batches(ticker=this.eth), 4)
        this.assertEqual(inventory.get_number_of_batches(market=this.binance), 5)
        this.assertEqual(inventory.get_number_of_batches(ticker=this.btc, market=this.gemini), 0)
        this.assertEqual(inventory.get_number_of_batches(ticker=this.eth, market=this.gemini), 2)
        this.assertEqual(inventory.get_number_of_batches(ticker=this.btc, market=this.binance), 3)
        this.assertEqual(inventory.get_number_of_batches(ticker=this.eth, market=this.binance), 2)

if __name__ == '__main__':
    unittest.main()
