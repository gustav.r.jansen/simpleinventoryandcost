from scic import HistoricalPrices

class MockHistoricalPrices(HistoricalPrices):
    def get_price(this, ticker, timestamp): return 1
