import datetime
import unittest
import scic

from mock_historical_prices import MockHistoricalPrices

class TestLogic(unittest.TestCase):
    def setUp(this):
        this.btc = scic.Ticker("btc")
        this.eth = scic.Ticker("eth")
        this.gemini = scic.Market("Gemini")
        this.binance = scic.Market("Binance")

        this.now = datetime.datetime.now(datetime.timezone.utc)
        this.now_p1 = this.now + datetime.timedelta(days=1)
        this.now_m1 = this.now - datetime.timedelta(days=1)
        this.now_m2 = this.now - datetime.timedelta(days=2)
        this.now_m3 = this.now - datetime.timedelta(days=3)


    def test_constructor(this):
        logic = scic.Logic(scic.TransactionRegistry(), scic.Inventory(), MockHistoricalPrices())

        this.assertIsNotNone(logic.registry)
        this.assertIsNotNone(logic.inventory)
        this.assertIsNotNone(logic.prices)

    def test_add_earn_to_inventory(this):
        logic = scic.Logic(scic.TransactionRegistry(), scic.Inventory(), MockHistoricalPrices())

        earn = scic.EarnTransaction(this.now, this.btc, this.gemini, 2.0)
        logic.apply_transaction_to_inventory(earn)

        this.assertEqual(logic.inventory.get_number_of_batches(), 1)
        this.assertEqual(logic.inventory.get_number_of_batches(market=this.gemini, ticker=this.btc), 1)

        batches = logic.create_batches_from_earn(earn)
        this.assertEqual(batches[0].timestamp, earn.timestamp)
        this.assertEqual(batches[0].ticker, earn.ticker)
        this.assertEqual(batches[0].market, earn.market)
        this.assertEqual(batches[0].amount, earn.amount)
        this.assertEqual(batches[0].unit_cost, 1)
        this.assertEqual(batches[0].get_cost(), earn.amount)

    def test_remove_lost_from_inventory(this):
        logic = scic.Logic(scic.TransactionRegistry(), scic.Inventory(), MockHistoricalPrices())

        lost = scic.LostTransaction(this.now, this.btc, this.gemini, 2.0)
        with this.assertRaises(ValueError): logic.apply_transaction_to_inventory(lost)

        earn = scic.EarnTransaction(this.now, this.btc, this.gemini, 2.0)
        logic.apply_transaction_to_inventory(earn)
        logic.apply_transaction_to_inventory(lost)
        this.assertEqual(logic.inventory.get_number_of_batches(), 0)
        this.assertEqual(logic.inventory.get_number_of_batches(market=this.gemini, ticker=this.btc), 0)

        market, ticker, amount = logic.create_args_from_lost(lost)
        this.assertEqual(market, this.gemini)
        this.assertEqual(ticker, this.btc)
        this.assertAlmostEqual(amount, 2)

if __name__ == '__main__':
    unittest.main()
