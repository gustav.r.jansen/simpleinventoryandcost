import unittest
import scic
import datetime

class TestInventoryBatch(unittest.TestCase):
    def test_inventory_batch_constructor(this):
        now = datetime.datetime.now(datetime.timezone.utc)
        ticker = scic.Ticker("btc")
        amount = 10.44
        unit_cost = 35034.45
        market = scic.Market("Gemini")

        batch = scic.InventoryBatch(now, ticker, amount, unit_cost, market)
        this.assertTrue(batch.timestamp==now)
        this.assertTrue(batch.ticker==ticker)
        this.assertTrue(batch.amount==amount)
        this.assertTrue(batch.unit_cost==unit_cost)
        this.assertTrue(batch.market==market)
        this.assertTrue(batch.removable)
        this.assertEqual(batch.id, 1)

        batch = scic.InventoryBatch(now, ticker, amount, unit_cost, market, False)
        this.assertEqual(batch.id, 2)
        this.assertFalse(batch.removable)

    def test_get_cost(this):
        batch = scic.InventoryBatch(datetime.datetime.now(datetime.timezone.utc), \
                scic.Ticker("btc"), 10.44, 35034.45, scic.Market("Gemini"))
        this.assertTrue(batch.get_cost()==10.44*35034.45)

    def test_remove_amount(this):
        batch = scic.InventoryBatch(datetime.datetime.now(datetime.timezone.utc), \
                scic.Ticker("btc"), 10.44, 35034.45, scic.Market("Gemini"))

        value = batch.remove_amount(5.44)
        this.assertTrue(value==0)
        this.assertAlmostEqual(batch.get_cost(),5*batch.unit_cost)

        value = batch.remove_amount(7.5)
        this.assertAlmostEqual(value,2.5)
        this.assertEqual(batch.get_cost(),0)

        batch = scic.InventoryBatch(datetime.datetime.now(datetime.timezone.utc), \
                scic.Ticker("btc"), 10.44, 35034.45, scic.Market("Gemini"), False)
        value = batch.remove_amount(5.44)
        this.assertTrue(value==5.44)
        this.assertTrue(batch.amount==10.44)

    def test_remove_value(this):
        batch = scic.InventoryBatch(datetime.datetime.now(datetime.timezone.utc), \
                scic.Ticker("btc"), 10.44, 35034.45, scic.Market("Gemini"))

        value = batch.remove_value(5.44*35034.45)
        this.assertTrue(value==0)
        this.assertAlmostEqual(batch.amount, 5)

        value = batch.remove_value(7.5*35034.45)
        this.assertAlmostEqual(value,2.5*35034.45)
        this.assertEqual(batch.get_cost(),0)

        batch = scic.InventoryBatch(datetime.datetime.now(datetime.timezone.utc), \
                scic.Ticker("btc"), 10.44, 35034.45, scic.Market("Gemini"), False)
        value = batch.remove_value(5.44*35034.45)
        this.assertEqual(value,5.44*35034.45)
        this.assertEqual(batch.get_cost(),10.44*35034.45)
if __name__ == '__main__':
    unittest.main()
